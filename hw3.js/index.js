'use strict'

let firstNumber = +prompt("Enter first number");

while (Number.isNaN(firstNumber) || !firstNumber) {
    alert(`Enter right first number`);
    firstNumber = +prompt(`Enter first number one more time`);

}

let sign = prompt("Enter sign");
if (sign !== "+" && sign !== "-" && sign !== "*" && sign !== "/") {
    sign = prompt("Enter sign one more time")
}

let secondNumber = +prompt("Enter right second number");
while (Number.isNaN(secondNumber) || !secondNumber) {
    alert(`Enter right second number`);
    secondNumber = +prompt(`Enter first number one more time`);

}

function calculate() {
    if (sign === '+') {
        return firstNumber + secondNumber;
    } else if (sign === '-') {
        return firstNumber - secondNumber;
    } else if (sign === '*') {
        return firstNumber * secondNumber;
    } else {
        return firstNumber / secondNumber;
    }
}

console.log(calculate());

