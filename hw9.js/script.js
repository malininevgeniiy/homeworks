const tabsTitle = document.body.querySelector('.tabs');
const tabsContent = document.body.querySelectorAll('.tabs-content li');

tabsTitle.addEventListener('click', (event) =>{
    const target = event.target;
    target.closest('ul').querySelector('.active').classList.remove('active');
    target.classList.add('active');
    tabsContent.forEach(item =>{
        item.setAttribute('hidden', 'true');
        if (target.dataset.names === item.dataset.text) {
            item.removeAttribute('hidden');
        }
    });
});
