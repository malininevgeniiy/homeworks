const items = ['hello', 'world', 23, '23', null];

const filterBy = (arr, type) => {
   return  arr.filter(item => typeof item !== type);
 }

console.log(filterBy(items, "string"));
//Цикл forEach перебирает весь масив и делает то, что прописано в функции, которая передаётся в аргумент.