const createNewUser = function () {
    const firstName = prompt('Enter your name');
    const lastName = prompt('Enter your surname');
    const today = new Date();
    const todayYear = today.getFullYear();
    const birth = prompt('Enter your date of birth (in format dd.mm.yyyy)');
    const birthYear = birth.slice(6, 11);


    const newUser = {
        firstName,
        lastName,
        birth,

        getAge() {
            const age = todayYear - birthYear;
            return age;
        },
        getLogin() {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();

        },

        getPassword() {
            return this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + birthYear;

        }

    };
    return newUser;


}
let start = createNewUser();
console.log(`Your login is: ${start.getLogin()}`);
console.log(`Your age is: ${start.getAge()}`);
console.log(`Your password is: ${start.getPassword()}`);



