const addImages = function () {
    const worksImagesAfterClick = document.querySelectorAll('.works-images-before-click');
    const loadImagesBtn = document.querySelector('.load-more-btn');
    worksImagesAfterClick.forEach(item => {
        item.style.display = 'block';
    });
    loadImagesBtn.remove();
}

const sectionWrapper = document.body.querySelector('.section-wrapper');
const tabsContent = document.body.querySelectorAll('.data-cont li');

sectionWrapper.addEventListener('click', (event) => {

    event.target.closest('ul').querySelector('.active').classList.remove('active');
    event.target.classList.add('active');
    tabsContent.forEach(item => {
        item.setAttribute('hidden', 'true');
        if (event.target.dataset.names === item.dataset.text) {
            item.removeAttribute('hidden');
        }
    });
});

const filterBox = document.querySelectorAll('.img-section');
document.querySelector('.works-section-wrapper').addEventListener('click', event => {
    if (event.target.tagName !== 'P') return false;
    const filterClass = event.target.dataset['text'];

    filterBox.forEach(item => {
        item.classList.remove('hide');
        if (!item.classList.contains(filterClass) && filterClass !== 'all') {
            item.classList.add('hide');
        }
    })
});

$(function () {
    $('.text-description').hide().first().show();
    $('.team-member').hide().first().show();
    $('.member-photo').hide().first().show();


    $('.slider-photo').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    $('.slider-arrow.right').click(function () {
        const activeIndex = $('.slider-photo.active').index() - 1;
        const newIndex = (activeIndex === 3) ? 0 : activeIndex + 1;
        $(`.slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    $('.slider-arrow.left').click(function () {
        const activeIndex = $('.slider-photo.active').index() - 1;
        const newIndex = (activeIndex === 0) ? 3 : activeIndex - 1;
        $(`.slider-photo:eq(${newIndex})`).addClass('active').siblings().removeClass('active');
        switchContent();
    })

    function switchContent() {
        const activeIndex = $('.slider-photo.active').index() - 1;
        $(`.text-description:eq(${activeIndex})`).show().siblings('.text-description').hide();
        $(`.team-member:eq(${activeIndex})`).show().siblings('.team-member').hide();
        $(`.member-photo:eq(${activeIndex})`).show().siblings('.member-photo').hide();
    }
})

