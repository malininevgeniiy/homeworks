const enterPassword = document.body.querySelector('.js-enter-password');
const confirmPassword = document.body.querySelector('.js-confirm-password');
const passwordForm = document.body.querySelector('.password-form');

passwordForm.addEventListener('click', (event)=>{
    if (event.target.classList.contains('fa-eye')  ) {
        event.target.classList.replace('fa-eye', 'fa-eye-slash');
        event.target.closest('label').childNodes[1].type = 'password' ;

    } else if (event.target.classList.contains('fa-eye-slash')) {
        event.target.classList.replace('fa-eye-slash', 'fa-eye');
        event.target.closest('label').childNodes[1].type = 'text' ;
    }

    console.log(event.target);
});
const checkPassword = function (){

    if (enterPassword.value !== confirmPassword.value || !enterPassword.value || !confirmPassword.value ) {
        const span = document.createElement('span');
        span.innerText = 'Нужно ввести одинаковые значения';
        span.style.color = 'red';
        document.body.append(span);

        enterPassword.onfocus = function (event) {
            span.innerText = '';
        };
        confirmPassword.onfocus = function (event) {
            span.innerText = '';
        };
    } else {
        alert('You are welcome');
        enterPassword.value = '';
        confirmPassword.value = '';
    }
};

