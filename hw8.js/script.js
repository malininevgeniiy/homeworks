const inputPrice = document.querySelector('.js-price-input');
inputPrice.onfocus = function (event){
    inputPrice.style.outline = 'none';
    inputPrice.style.border = 'solid 2px green';
};
inputPrice.onblur = function (event){
    const currentPrice = inputPrice.value;
    console.log(currentPrice);
    inputPrice.style.display = 'flex';
    if (currentPrice <= '0') {
        inputPrice.style.border = 'solid 3px red';
        inputPrice.after('Please enter correct price.');

    } else {
        const span = document.createElement('span');
        span.innerText = ('Текущая цена:' + ' ' + currentPrice + ' ' + 'USD.');
        const button = document.createElement("button");
        span.appendChild(button);
        button.innerText = 'X';
        button.addEventListener('click', () =>{
            span.remove();
        });
        inputPrice.style.border = 'solid 1px black';
        inputPrice.style.color = 'green';
        document.body.before(span);
    }
}
