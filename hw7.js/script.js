const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parent = document.getElementById('root');
const getArray = function (arr,parent = document.body) {
 const ul = document.createElement('ul');
 const newArray = arr.map((item) => {

   const li = document.createElement('li');
   li.innerHTML = item;
   return li;

 });
 newArray.forEach(item =>{
  ul.appendChild(item);
 });
 parent.appendChild(ul);
 console.log(newArray);
};
getArray(arr, parent);

//DOM это такое дерево, с помощью которого мы можем работать с html, css jsом, что добавляет функционала, например, при клике добавить какой-то текст с картинками и //оформлением.