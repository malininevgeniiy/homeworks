let name;
let age;

do name = prompt('Please, enter name');
while (name == "" );

do {
   age = +prompt('Please, enter age');
} while (Number.isNaN(age) == true || age == "");


if (age > 22){
    alert('Welcome, ' + name)
}else if (age <=22 && age >= 18 && confirm('Are you sure you want to continue?')){
    alert('Welcome, ' + name)
}else{
    alert('You are not allowed to visit this website.')
}

/* Объявлять переменную через const нужно тогда, когда мы её не будем перезаписывать в дальнейших действиях.
Если мы, например, в условии задаём переменную через let, мы не вытащим её за предели блока, с var же это возможно
Нежелательно объявлять переменные через var т.к они обрабатываются самыми первыми, но значене получают, только после строки с объявлением,
можно запутаться.
 */


