//Циклы нужны, что выполнять действие до тех пор, пока нашле условие не получит значение , которые нас устраивает. Например,
//Выводи окно(prompt), которое считывает данные, до тех пор, пока пользователь не вводит цифру.
//cost number;
//do {
//     number = +prompt('Please, enter number');
// } while (Number.isNaN(number) === true);
// Так же, что бы не повторять определённый набор действий, экономии памяти

let number = +prompt('Enter number');

while (!Number.isInteger(number)) {
    number = +prompt('Enter right number!');
}

if (number >= 5) {
    for (let i = 0; i <= number; i++) {
        if (i % 5 === 0) {
            console.log(i);

        }
    }
    console.log('Next task:');
} else {
    console.log('Sorry, no numbers');
    console.log('Next task:');
}

let lessNumber = +prompt('Enter less number.');
let moreNumber = +prompt('Enter more number.');

do {
    alert('You entered incorrect values')
    lessNumber = +prompt('Enter less number.');
    moreNumber = +prompt('Enter more number.');
} while (lessNumber > moreNumber);
for (lessNumber; lessNumber < moreNumber; lessNumber++) {
    console.log(lessNumber);
}